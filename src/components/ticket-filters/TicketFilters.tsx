import './TicketFilters.scss';
import {Checkbox} from "../checkbox";
import React, {useCallback, useEffect, useState} from "react";
import {TicketFilterType} from "../../types/ticket-filter-type";

interface TicketFiltersProps {
    onChange: (filters: TicketFilterType[]) => void;
}

export const TicketFilters: React.FC<TicketFiltersProps> = ({ onChange }) => {
    const [filters, setFilters] = useState<TicketFilterType[]>([]);

    const selectFilter = useCallback((filter: TicketFilterType) => {
        if (filters.includes(filter)) {
            const index = filters.indexOf(filter);
            const newFilters = [...filters];
            newFilters.splice(index, 1)
            setFilters(newFilters);
        } else {
            setFilters([...filters, filter]);
        }
    }, [filters]);

    useEffect(() => {
        onChange(filters);
    }, [filters, onChange]);

    return (
        <div className='ticket-filter__wrapper'>
            <h3 className='ticket-filter__title'>
                Количество пересадок
            </h3>
            <Checkbox title='Все' onChange={() => selectFilter(TicketFilterType.ALL)}/>
            <Checkbox title='Нет пересадок' onChange={() => selectFilter(TicketFilterType.WITHOUT)}/>
            <Checkbox title='1 пересадка' onChange={() => selectFilter(TicketFilterType.ONE)}/>
            <Checkbox title='2 пересадки' onChange={() => selectFilter(TicketFilterType.TWO)}/>
            <Checkbox title='3 пересадки' onChange={() => selectFilter(TicketFilterType.THREE)}/>
        </div>
    );
};
