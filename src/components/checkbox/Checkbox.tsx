import './Checkbox.scss';
import React, {ChangeEvent, useState} from "react";

interface CheckboxProps {
    title: string;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const Checkbox: React.FC<CheckboxProps> = ({ title , onChange }) => {
    const [value, setValue] = useState(false);

    return (
        <label className='checkbox'>
            <input checked={value} onChange={(e) => {
                setValue(!value);
                onChange(e);
            }} type="checkbox"/>
            <span className='checkbox__mark'>
                <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M4.28571 8L0 4.16123L1.20857 3.0787L4.28571 5.82726L10.7914 0L12 1.09021L4.28571 8Z" fill="#2196F3"/>
                </svg>
            </span>
            <span className='checkbox__title'>{ title }</span>
        </label>
    );
}
