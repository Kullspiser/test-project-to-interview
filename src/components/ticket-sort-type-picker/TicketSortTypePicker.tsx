import './TicketSortTypePicker.scss';
import React, {useCallback, useEffect, useState} from "react";
import {TicketSortType} from "../../types";
import classNames from 'classnames';

interface TicketSortTypePickerProps {
    onChange: (ticketType: TicketSortType) => void;
}

export const TicketSortTypePicker: React.FC<TicketSortTypePickerProps> = ({ onChange }) => {
    const [selectedType, setSelectedType] = useState<TicketSortType>(TicketSortType.CHEAP);

    useEffect(() => {
        onChange(selectedType);
    }, [selectedType, onChange]);

    const getClasses = useCallback((type: TicketSortType) => classNames({
        'ticket-sort-type-picker__option': true,
        'ticket-sort-type-picker__option--selected': selectedType === type,
    }), [selectedType]);

    const selectType = useCallback((type: TicketSortType) => {
        setSelectedType(type);
    }, []);

    return (
        <div className='ticket-sort-type-picker'>
            <button onClick={() => selectType(TicketSortType.CHEAP)} className={getClasses(TicketSortType.CHEAP)}>
                Самый дешевый
            </button>
            <button onClick={() => selectType(TicketSortType.FAST)} className={getClasses(TicketSortType.FAST)}>
                Самый быстрый
            </button>
        </div>
    );
}
