import React, {useEffect, useState} from "react";
import axios from "axios";
import {SearchIdResponse} from "../../types";

interface SearchIdContextProviderValue {
    searchId: string | null;
}

export const SearchId = React.createContext<SearchIdContextProviderValue>({
    searchId: null,
})

export const SearchIdContextProvider: React.FC = ({ children }) => {
    const [searchId, setSearchId] = useState<string | null>(null);

    useEffect(() => {
        axios.get<SearchIdResponse>('/api/search').then(res => {
            setSearchId(res.data.searchId);
        })
    }, []);

    return (
        <SearchId.Provider value={{ searchId,}}>
            { children }
        </SearchId.Provider>
    )
};
