import './TicketCard.scss';
import {getDateTripAsString, getPublicFolderPath, getTripDurationAsString} from "../../utils";
import {Ticket} from "../../types";
import plural from 'plural-ru';

interface TickerCardProps {
    ticket: Ticket;
}

export const TickerCard: React.FC<TickerCardProps> = ({ ticket }) => {
    return (
        <div className='ticket-card'>
            <div className="ticket-card__header">
                <h4 className='ticket-card__title'>
                    {ticket.price} Р
                </h4>
                <img
                    className='ticket-card__company-logo'
                    src={`${getPublicFolderPath()}/img/sample_logo.png`}
                    alt="Company logo"/>
            </div>
            <div className="ticker-card__content">
                { ticket.segments.map(s => (
                    <div key={s.duration + s.date + s.origin + s.destination} className="ticket-card__content-item">
                        <div>
                            <span className='ticket-card__content-item-label'>
                                {s.origin} – {s.destination}
                            </span>
                                <span className='ticket-card__content-item-description'>
                                { getDateTripAsString(s.date, s.duration) }
                            </span>
                        </div>
                        <div>
                            <span className='ticket-card__content-item-label'>
                                В пути
                            </span>
                                <span className='ticket-card__content-item-description'>
                                { getTripDurationAsString(s.duration) }
                            </span>
                        </div>
                        <div>
                            <span className='ticket-card__content-item-label'>
                                { plural(s.stops.length, '%d пересадка', '%d пересадки', '%d пересадок') }
                            </span>
                            <span className='ticket-card__content-item-description'>
                                { s.stops.length ? s.stops.join(', ') : '-' }
                            </span>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}
