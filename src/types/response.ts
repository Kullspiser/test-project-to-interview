import {Ticket} from "./ticket";

export interface PoolingResponse {
    stop: boolean;
}

export interface SearchIdResponse {
    searchId: string;
}

export interface TicketResponse extends PoolingResponse {
    tickets: Ticket[];
}
