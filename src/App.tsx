import React from 'react';
import { PrimaryTicketsPage } from './pages/primary-tickets-page';
import './App.scss';
import {SearchIdContextProvider} from "./components/search-id-context";

function App() {
  return (
    <div className="test-app__container">
      <div className="test-app__wrapper">
          <SearchIdContextProvider>
              <PrimaryTicketsPage />
          </SearchIdContextProvider>
      </div>
    </div>
  );
}

export default App;
