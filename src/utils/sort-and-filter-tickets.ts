import {Ticket, TicketSortType} from "../types";
import {TicketFilterType} from "../types/ticket-filter-type";

type SortTicketCallback = (a: Ticket, b: Ticket) => number;

export const sortTickets = (sortType: TicketSortType): SortTicketCallback => {
    if (sortType === TicketSortType.CHEAP) {
        return (a, b) => (
            a.price - b.price
        );
    }

    if (sortType === TicketSortType.FAST) {
        return (a, b) => (
            a.segments.reduce((acc, s) => s.duration + acc, 0) -
            b.segments.reduce((acc, s) => s.duration + acc, 0)
        );
    }

    return (a, b) => (
        a.price - b.price
    );
}

export const filterTickets = (filterTypes: TicketFilterType[], tickets: Ticket[]): Ticket[] => {
    console.log(filterTypes);
    const res: Ticket[] = [];

    if (filterTypes.length === 0) {
        return tickets;
    }

    if (filterTypes.includes(TicketFilterType.ALL)) {
        return tickets;
    }

    const numericFilter = {
        [TicketFilterType.ONE]: 1,
        [TicketFilterType.TWO]: 2,
        [TicketFilterType.THREE]: 3
    }

    for (const t of tickets) {
        const stops = t.segments.reduce((acc, s) => acc + s.stops.length, 0);

        let resolvers = 0;

        for (const filter of filterTypes) {
            if ((
                    filter === TicketFilterType.ONE
                    || filter === TicketFilterType.TWO
                    || filter === TicketFilterType.THREE)
                && stops === numericFilter[filter]
            ) {
                resolvers++;
            }

            if (filter === TicketFilterType.WITHOUT && stops === 0) {
                resolvers++;
            }
        }

        if (resolvers > 0) {
            res.push(t);
        }
    }

    return res;
}
